<?php declare(strict_types = 1);

namespace Process\Module\Test\Component;

final class CallableFactory
{
    public function __invoke(): callable
    {
        return static function (): void {
        };
    }
}
