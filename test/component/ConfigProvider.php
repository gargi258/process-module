<?php declare(strict_types = 1);

namespace Process\Module\Test\Component;

final class ConfigProvider
{
    /** @return mixed[] */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->dependencies(),
            'process' => $this->sagaDeclaration(),
        ];
    }

    /** @return mixed[] */
    public function dependencies(): array
    {
        return [
            'factories' => [
                'simple-saga-close-action' => \Process\Module\Test\Component\CallableFactory::class,
                'simple-saga-close-strategy' => \Process\Module\Test\Component\CallableFactory::class,
            ],
        ];
    }

    /** @return mixed[] */
    public function sagaDeclaration(): array
    {
        return [
            'saga' => [
                'simple-saga' => [
                    'initialEventName' => 'InitialEvent',
                    'listeningEventList' => [],
                    'compensationEventList' => [],
                    'closeSagaActionName' => 'simple-saga-close-action',
                    'closeSagaStrategyName' => 'simple-saga-close-strategy',
                    'handlers' => [],
                    'compensators' => [],
                ],
            ],
            'event' => [
                'stream' => [
                    'repository' => [
                        'pdo' => [
                            'dsn' => 'pgsql:host=database;dbname=devdb',
                            'username' => 'devdb',
                            'password' => 'devdb',
                        ],
                    ],
                ],
            ],
        ];
    }
}
