<?php declare(strict_types = 1);

namespace Process\Module\Test\Component\Saga\Builder\Container;

use PHPUnit\Framework\TestCase;
use Process\Event\Stream\Repository\Pdo;
use Process\Module\ConfigProvider;
use Process\Module\Test\Component\ConfigProvider as TestConfigProvider;
use Process\Processor\Builder;
use Process\Processor\Saga\Builder\Container;
use Process\Saga\Config;
use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ServiceManager\ServiceManager;

class ServiceManagerTest extends TestCase
{
    /** @var ServiceManager */
    private $container;

    protected function setUp(): void
    {
        $aggregator = new ConfigAggregator([
            ConfigProvider::class,
            TestConfigProvider::class,
        ]);
        $config = $aggregator->getMergedConfig();
        $dependencies = $config['dependencies'];
        $dependencies['services']['config'] = $config;

        $container = new ServiceManager($dependencies);

        $this->container = $container;
    }

    /** @test */
    public function whenGetSagaBuilderThenProperBuilderIsReturned(): void
    {
        $builder = $this->container->get('simple-saga');

        self::assertEquals($this->expectedBuilder(), $builder);
    }

    private function expectedBuilder(): Builder
    {
        return new Container(
            new Config(
                'InitialEvent',
                [],
                [],
                'simple-saga-close-action',
                'simple-saga-close-strategy'
            ),
            $this->container->get(Pdo::class),
            $this->container
        );
    }
}
