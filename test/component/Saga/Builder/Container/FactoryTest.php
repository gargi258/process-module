<?php declare(strict_types = 1);

namespace Process\Module\Test\Component\Saga\Builder\Container;

use PHPUnit\Framework\TestCase;
use Process\Event\Stream;
use Process\Module\ConfigProvider;
use Process\Module\Saga\Builder\Container\Factory;
use Process\Module\Test\Component\ConfigProvider as TestConfigProvider;
use Process\Processor\Builder;
use Process\Processor\Saga\Builder\Container;
use Process\Saga\Config;
use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ServiceManager\ServiceManager;

class FactoryTest extends TestCase
{
    /** @var ServiceManager */
    private $container;
    /** @var Stream\Repository */
    private $streams;
    /** @var Factory */
    private $factory;

    protected function setUp(): void
    {
        $aggregator = new ConfigAggregator([
            ConfigProvider::class,
            TestConfigProvider::class,
        ]);
        $config = $aggregator->getMergedConfig();
        $dependencies = $config['dependencies'];
        $dependencies['services']['config'] = $config;

        $container = new ServiceManager($dependencies);
        /** @var Stream\Repository\Pdo $streams */
        $this->streams = $container->get(Stream\Repository\Pdo::class);

        $this->container = $container;
        $this->factory = new Factory();
    }

    /** @test */
    public function whenConfigurationExistThenCanCreateReturnTrue(): void
    {
        $canCreate = $this->factory->canCreate(
            $this->container,
            'simple-saga'
        );

        self::assertTrue($canCreate);
    }

    /** @test */
    public function whenConfigurationDoesNotExistThenCanCreateReturnFalse(): void
    {
        $canCreate = $this->factory->canCreate(
            $this->container,
            'non-exist-saga'
        );

        self::assertFalse($canCreate);
    }

    /** @test */
    public function whenConfigurationExistThenReturnSagaBuilder(): void
    {
        $factory = $this->factory;

        /** @var Builder $saga */
        $builder = $factory($this->container, 'simple-saga');

        self::assertEquals($this->expectedBuilder(), $builder);
    }

    private function expectedBuilder(): Builder
    {
        return new Container(
            new Config(
                'InitialEvent',
                [],
                [],
                'simple-saga-close-action',
                'simple-saga-close-strategy'
            ),
            $this->streams,
            $this->container
        );
    }
}
