<?php declare(strict_types = 1);

// phpcs:disable Generic.Files.LineLength.TooLong

namespace Process\Module;

final class ConfigProvider
{
    /** @return mixed[] */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'process' => $this->getLibraryConfiguration(),
        ];
    }

    /** @return mixed[] */
    private function getDependencies(): array
    {
        return [
            'abstract_factories' => [
                \Process\Module\Saga\Builder\Container\Factory::class,
            ],
            'factories' => [
                \Process\Event\Stream\Repository\InMemory::class => \Zend\ServiceManager\Factory\InvokableFactory::class,
                \Process\Event\Stream\Repository\Pdo::class => \Process\Module\Saga\Event\Stream\Repository\Pdo\Factory::class,
                \Process\Saga\Event\Resolver\Container::class => \Process\Module\Saga\Event\Resolver\Container\Factory::class,
                \Process\Event\Builder\Reflection::class => \Zend\ServiceManager\Factory\InvokableFactory::class,
                \Process\Event\Builder\DummyArray::class => \Zend\ServiceManager\Factory\InvokableFactory::class,
            ],
            'aliases' => [
                \Process\Event\Stream\Repository::class => \Process\Event\Stream\Repository\Pdo::class,
                \Process\Saga\Event\Resolver\Handler::class => \Process\Saga\Event\Resolver\Container::class,
                \Process\Saga\Event\Resolver\Compensator::class => \Process\Saga\Event\Resolver\Container::class,
                \Process\Event\Builder::class => \Process\Event\Builder\DummyArray::class,
            ],
        ];
    }

    /** @return mixed[] */
    private function getLibraryConfiguration(): array
    {
        return [
            'saga' => [], // configuration of each saga
            'event' => [
                'stream' => [
                    'repository' => [
                        'pdo' => [
                            'dsn' => null,
                            'username' => null,
                            'password' => null,
                        ],
                    ],
                ],
            ],
        ];
    }
}
