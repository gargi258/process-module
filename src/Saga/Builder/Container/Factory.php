<?php declare(strict_types = 1);

// phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
// phpcs:disable SlevomatCodingStandard.TypeHints.NullableTypeForNullDefaultValue.NullabilitySymbolRequired
// phpcs:disable SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
// phpcs:disable SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingReturnTypeHint

namespace Process\Module\Saga\Builder\Container;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Process\Event\Stream\Repository as Streams;
use Process\Processor\Saga\Builder\Container;
use Process\Saga\Config;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

final class Factory implements AbstractFactoryInterface
{
    /**
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        $config = $container->get('config');

        return isset($config['process']['saga'][$requestedName]);
    }

    /**
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  mixed[]|null       $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {
        $config = $container->get('config');

        $sagaConfig = $config['process']['saga'][$requestedName];

        return new Container(
            new Config(
                $sagaConfig['initialEventName'],
                $sagaConfig['listeningEventList'],
                $sagaConfig['compensationEventList'],
                $sagaConfig['closeSagaActionName'],
                $sagaConfig['closeSagaStrategyName']
            ),
            $container->get(Streams::class),
            $container
        );
    }
}
