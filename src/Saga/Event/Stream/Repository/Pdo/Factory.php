<?php declare(strict_types = 1);

namespace Process\Module\Saga\Event\Stream\Repository\Pdo;

use Process\Event\Builder;
use Process\Event\Stream\Repository\Pdo;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container): Pdo
    {
        $config = $container->get('config');
        $pdoConfig = $config['process']['event']['stream']['repository']['pdo'];

        $pdo = new \PDO(
            $pdoConfig['dsn'],
            $pdoConfig['username'],
            $pdoConfig['password']
        );
        /** @var Builder $eventBuilder */
        $eventBuilder = $container->get(Builder::class);

        return new Pdo($pdo, $eventBuilder);
    }
}
