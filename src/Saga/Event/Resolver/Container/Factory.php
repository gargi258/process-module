<?php declare(strict_types = 1);

namespace Process\Module\Saga\Event\Resolver\Container;

use Process\Saga\Event\Resolver\Container;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container): Container
    {
        $config = $container->get('config');

        $sagas = $config['process']['saga'];

        if ([] === $sagas) {
            return new Container($container, [], []);
        }

        $handlers = [];
        $compensators = [];

        foreach ($sagas as $name => $sagaConfig) {
            $handlers[$name] = $sagaConfig['handlers'];
            $compensators[$name] = $sagaConfig['compensators'];
        }

        return new Container($container, $handlers, $compensators);
    }
}
